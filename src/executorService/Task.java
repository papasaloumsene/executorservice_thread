package executorService;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Task implements Callable<Integer>{

	public static void main(String[] args) throws InterruptedException {
		ExecutorService service = Executors.newFixedThreadPool(5);
		
		Task task = new Task();
		
		
        //in this case we have 50 futures with 50 placeholder
		List<Future> futures = new ArrayList<>();
		for(int i = 0; i <=10; i ++){
		Future<Integer> future = service.submit(new Task());
		futures.add(future);
		}
		
		for(int i = 0; i <=10; i ++){
			Future<Integer> future = futures.get(i);
			try {
				Integer result= future.get();
				System.out.println("resulte of future #" + i +" ="+result);
			} catch (InterruptedException | ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		 System.out.println("end of first thread: " +Thread.currentThread().getName());
		 
		 
		 ExecutorService secondservice = Executors.newFixedThreadPool(3);
		 
		 //in this case we have 50 futures with 50 placeholder
		List<Future> second_Futeres = new ArrayList<>();
			for(int j= 10; j>=0; j--) {
				Future<Integer> second_future= secondservice.submit(new SecondTask());
				second_Futeres.add(second_future);
			}
			
			for(int j = 10; j >=0; j --){
				Future<Integer> secondfuture = second_Futeres.get(j);
				try {
					Integer secondresult= secondfuture.get();
					System.out.println("the secondresulte of future #" + j +" ="+secondresult);
				} catch (InterruptedException | ExecutionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			
			
	     System.out.println("end  of the second thread: " +Thread.currentThread().getName());
		
         
	    }

	@Override
	public Integer call() throws Exception {		
		
		Thread.sleep(3000);
		return new Random().nextInt();
	}
	
	
  

}
